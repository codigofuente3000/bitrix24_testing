<?php

return [

    'base_uri' => '',

    'client_domain' => '',

    'client_id'     => '',

    'client_secret' => '',

    'username'      => '',

    'credentials'   => [],

];

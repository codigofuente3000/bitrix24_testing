<?php

namespace App\Http\Controllers;

use App\Repositories\Bitrix24\DealCategories;
use App\Repositories\Bitrix24\Deals;
use App\Repositories\Bitrix24\Tasks;
use App\Repositories\Bitrix24\Telephony;
use App\Repositories\Bitrix24\Users;
use Illuminate\Http\Request;

class TestController extends Controller
{
	protected $deals;
    protected $tasks;
    protected $telephony;
	protected $users;
	protected $deal_categories;


    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Users $users, Deals $deals, DealCategories $deal_categories, Tasks $tasks, Telephony $telephony)
    {
    	$this->deals = $deals;
    	$this->deal_categories = $deal_categories;
        $this->tasks = $tasks;
        $this->telephony = $telephony;
        $this->users = $users;
    }

    public function index()
    {
        $data = $this->deals->get(1);

        dd($data);

        $data = $this->telephony->externalcall_register(24, 1009, 212);
        
        $data = $this->telephony->externalcall_show($data['result']['CALL_ID'], 24);

        dd($data);
    }
}

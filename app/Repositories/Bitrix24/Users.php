<?php

namespace App\Repositories\Bitrix24;

use App\Traits\GuzzleHttpRequest;

class Users
{
    use GuzzleHttpRequest;

    /**
     * Retrieves list of user field names.
     *
     * @return array
     */
    public function fields()
    {
        return $this->getRequest('user.fields');
    }

    /**
     * Get current user information. Method supports secure calling.
     *
     * @return array
     */
    public function current()
    {
        return $this->getRequest('user.current');
    }

    /**
     * Invites a user. Available only for users with invitation permissions.
     * If successful, the user will be sent a standard invitation to the portal.
     *
     * @param
     * 
     * If it is required to add an extranet user, then the following shall be passed in the fields: EXTRANET: Y
     * and SONET_GROUP_ID: [...]. If it is required to add an intranet user,
     * then the following shall be passed: UF_DEPARTMENT: [...].
     *
     * @return array
     */
    public function add(array $fields = ['EMAIL' => null])
    {
        $parameters['fields'] = $fields;

        return $this->getRequest('user.add', $parameters);
    }

    /**
     * Retrieves filtered list of users. This method will return all users, except: bots, users for e-mail and users for Open Channels.
     *
     * @return array
     */
    public function get(array $filter = [], string $sort = 'ID', string $order = 'ASC')
    {
        $parameters['filter'] = $filter;
        $parameters['sort'] = $sort;
        $parameters['order'] = $order;

        return $this->getRequest('user.get', $parameters);
    }

    /**
     * This method is used to retrieve list of users with expedited personal data search
     * (name, last name, middle name, name of department, position).
     * Works in two modes: Quick mode, via Fulltext Index
     * and slower mode via right LIKE (support is determined automatically).
     *
     * @param
     *
     * @return array
     */
    public function search(array $fields)
    {
        $parameters['fields'] = $fields;

        return $this->getRequest('user.search', $parameters);
    }

    /**
     * Updates user information. Available only for users with invitation permissions.
     *
     * @param
     * 
     * All user.fields. ID field is required. Cannot change EMAIL.
     *
     * @return array
     */
    public function update(array $fields = ['ID' => null])
    {
        $parameters['fields'] = $fields;

        return $this->getRequest('user.update', $parameters);
    }


}
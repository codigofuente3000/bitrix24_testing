<?php

namespace App\Repositories\Bitrix24;

use App\Traits\GuzzleHttpRequest;

class DealCategories
{
    use GuzzleHttpRequest;

    /**
     * Creates a new deal category.
     *
     * @return array
     */
    public function add(array $fields)
    {
        $parameters['fields'] = $fields;

        return $this->getRequest('crm.dealcategory.add', $parameters);
    }

    /**
     * Deletes a deal category. 
     *
     * @return boolean
     */
    public function delete(int $id)
    {
        $parameters['id'] = $id;

        return $this->getRequest('crm.dealcategory.delete', $parameters);
    }

    /**
     * Returns field description for deal categories.
     *
     * @return array
     */
    public function fields()
    {
        return $this->getRequest('crm.dealcategory.fields');
    }

    /**
     * Returns deal category by the ID. 
     *
     * @return array
     */
    public function get(int $id)
    {
        $parameters['id'] = $id;

        return $this->getRequest('crm.dealcategory.get', $parameters);
    }

    /**
     * Returns a list of deal categories by the filter. Is the implementation of list method for deal categories.
     *
     * @return array
     */
    public function list(array $filter = [], array $select = ['*'], array $order = ['ID' => 'ASC'])
    {
        $parameters['filter'] = $filter;
        $parameters['select'] = $select;
        $parameters['order'] = $order;

        return $this->getRequest('crm.dealcategory.list', $parameters);
    }

    /**
     * Returns directory type ID for staorage deal categories by the ID. 
     * This string is of the DEAL_STAGE_[category ID] type.
     * For example, for category with ID 1, "DEAL_STAGE_1" string will be returned.
     * The ID is designed to work with the family of crm.status.* methods.
     * For example, to create a new stage for a category,
     * the ID must be passed to the crm.status.add method as the ENTITY_ID parameter.
     *
     * @return array
     */
    public function status(int $id)
    {
        $parameters['id'] = $id;

        return $this->getRequest('crm.dealcategory.status', $parameters);
    }

    /**
     * Updates an existing category.
     *
     * @return array
     */
    public function update(int $id, array $fields)
    {
        $parameters['id'] = $id;
        $parameters['fields'] = $fields;

        return $this->getRequest('crm.dealcategory.update', $parameters);
    }

    /**
     * Returns a list of deal stages for category by the ID. Equivalent to calling crm.status.list method with parameter ENTITY_ID
     * equal to the result of calling crm.dealcategory.status method.
     *
     * @return array
     */
    public function stage_list(int $id)
    {
        $parameters['id'] = $id;

        return $this->getRequest('crm.dealcategory.stage.list', $parameters);
    }


}
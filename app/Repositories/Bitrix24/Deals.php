<?php

namespace App\Repositories\Bitrix24;

use App\Traits\GuzzleHttpRequest;

class Deals
{
    use GuzzleHttpRequest;

    /**
     * The method creates a new deal.
     *
     * @return array
     */
    public function add(array $fields, array $params = ['REGISTER_SONET_EVENT' => 'Y'])
    {
        $parameters['fields'] = $fields;
        $parameters['params'] = $params;

        return $this->getRequest('crm.deal.add', $parameters);
    }

    /**
     * Deletes the specified deal and all the associated objects.
     *
     * @return array
     */
    public function delete(int $id)
    {
        $parameters['id'] = $id;

        return $this->getRequest('crm.deal.delete', $parameters);
    }

    /**
     * Returns the description of the deal fields, including user fields.
     *
     * @return array
     */
    public function fields()
    {
        return $this->getRequest('crm.deal.fields');
    }

    /**
     * Returns a deal by the deal ID.
     *
     * @return array
     */
    public function get(int $id)
    {
        $parameters['id'] = $id;

        return $this->getRequest('crm.deal.get', $parameters);
    }

    /**
     * Returns a list of deals selected by the filter specified as the parameter.
     *
     * @return array
     */
    public function list(array $filter = [], array $select = ['*'], array $order = ['ID' => 'ASC'])
    {
        $parameters['filter'] = $filter;
        $parameters['select'] = $select;
        $parameters['order'] = $order;

        return $this->getRequest('crm.deal.list', $parameters);
    }

    /**
     * Updates the specified (existing) deal.
     *
     * @return boolean
     */
    public function update(int $id, array $fields, array $params = ['REGISTER_SONET_EVENT' => 'Y'])
    {
        $parameters['id'] = $id;
        $parameters['fields'] = $fields;
        $parameters['params'] = $params;

        return $this->getRequest('crm.deal.update', $parameters);
    }

    /**
     * Deletes contact from a specified deal.
     *
     * @return boolean
     */
    public function contact_delete(int $id, array $fields = ['CONTACT_ID' => null])
    {
        $parameters['id'] = $id;
        $parameters['fields'] = $fields;

        return $this->getRequest('crm.deal.contact.delete', $parameters);
    }

    /**
     * Adds contact to specified deal.
     *
     * @return array
     */
    public function contact_add(int $id, array $fields = ['CONTACT_ID' => null, 'SORT' => null, 'IS_PRIMARY' => null])
    {
        $parameters['id'] = $id;
        $parameters['fields'] = $fields;

        return $this->getRequest('crm.deal.contact.add', $parameters);
    }

    /**
     * Returns field descriptions for the deal-contact link used by methods of family crm.deal.contact.*,
     * that is by crm.deal.contact.items.get, crm.deal.contact.items.set, crm.deal.contact.add and etc.
     *
     * @return array
     */
    public function contact_fields()
    {
        return $this->getRequest('crm.deal.contact.fields');
    }

    /**
     * Clears a set of contacts, associated with the specified deal.
     *
     * @return boolean
     */
    public function contact_items_delete(int $id)
    {
        $parameters['id'] = $id;

        return $this->getRequest('crm.deal.contact.items.delete', $parameters);
    }

    /**
     * Returns a set of contacts, associated with the specified deal. 
     *
     * @return array
     */
    public function contact_items_get(int $id)
    {
        $parameters['id'] = $id;

        return $this->getRequest('crm.deal.contact.items.get', $parameters);
    }

    /**
     * Returns a set of contacts, associated with the specified seal.
     *
     * @return array
     */
    public function contact_items_set(int $id, array $items = ['CONTACT_ID' => null, 'SORT' => null, 'IS_PRIMARY' => null])
    {
        $parameters['id'] = $id;
         $parameters['items'] = $items;

        return $this->getRequest('crm.deal.contact.items.set', $parameters);
    }

    /**
     * Returns products inside the specified deal.
     *
     * @return array
     */
    public function productrows_get(int $id)
    {
        $parameters['id'] = $id;

        return $this->getRequest('crm.deal.productrows.get', $parameters);
    }

    /**
     * Creates or updates product entries inside the specified deal.
     *
     * @return array
     */
    public function productrows_set(int $id, array $rows)
    {
        $parameters['id'] = $id;
        $parameters['rows'] = $rows;

        return $this->getRequest('crm.deal.productrows.set', $parameters);
    }

    /**
     * Created new user field for deals.
     * System limitation for field name - 20 symbols. User field name is supplemented by prefix UF_CRM_,
     * that is the real name length - 13 symbols.
     *
     * @return array
     */
    public function userfield_add(array $fields)
    {
        $parameters['fields'] = $fields;

        return $this->getRequest('crm.deal.userfield.add', $parameters);
    }

    /**
     * Deleted user deals field.
     *
     * @param int $id - User field ID
     *
     * @return boolean
     */
    public function userfield_delete(int $id)
    {
        $parameters['id'] = $id;

        return $this->getRequest('crm.deal.userfield.delete', $parameters);
    }

    /**
     * Returns user deals field by deal ID.
     *
     * @return array
     */
    public function userfield_get(int $id)
    {
        $parameters['id'] = $id;

        return $this->getRequest('crm.deal.userfield.get', $parameters);
    }

    /**
     * Returns list of user deal fields by filter.
     *
     * @return array
     */
    public function userfield_list(array $filter = [], array $order = ['ID' => 'ASC'])
    {
        $parameters['filter'] = $filter;
        $parameters['order'] = $order;

        return $this->getRequest('crm.deal.userfield.list', $parameters);
    }

    /**
     * Updates existing user deals field.
     *
     * @return boolean
     */
    public function userfield_update(int $id, array $fields)
    {
        $parameters['id'] = $id;
        $parameters['fields'] = $fields;

        return $this->getRequest('crm.deal.userfield.update', $parameters);
    }


}
<?php

namespace App\Repositories\Bitrix24;

use App\Traits\GuzzleHttpRequest;

class Tasks
{
    use GuzzleHttpRequest;

    /**
     * Creates a new task. Returns ID of added task. The following fields are available.
     *
     * @return array
     */
    public function item_add(array $fields)
    {
        $parameters['fields'] = $fields;

        return $this->getRequest('task.item.add', $parameters);
    }

    /**
     * This method loads file for a task. Presently file loading is realized
     * via post with the transfer of the file in the CONTENT parameter.
     *
     * @return array
     */
    public function item_addfile(int $taskid, string $name, string $content)
    {
        $parameters['TASK_ID'] = $taskid;
        $parameters['NAME'] = $taskid;
        $parameters['CONTENT'] = $content;

        return $this->getRequest('task.item.addfile', $parameters);
    }

    /**
     * This method adds task to Favourites. 
     *
     * @return array
     */
    public function item_addtofavorite(int $taskid, array $params)
    {
        $parameters['TASK_ID'] = $taskid;
        $parameters['PARAMS'] = $params;

        return $this->getRequest('task.item.addtofavorite', $parameters);
    }

    /**
     * Changes task status from waiting for confirmation to Completed.
     *
     * @return array
     */
    public function item_approve(int $taskid)
    {
        $parameters['taskid'] = $taskid;

        return $this->getRequest('task.item.approve', $parameters);
    }

    /**
     * Changes status to Completed or Supposedly completed (requires creator's attention).
     *
     * @return array
     */
    public function item_complete(int $taskid)
    {
        $parameters['taskid'] = $taskid;

        return $this->getRequest('task.item.complete', $parameters);
    }

    /**
     * Changes task status to Deferred.
     *
     * @return array
     */
    public function item_defer(int $taskid)
    {
        $parameters['taskid'] = $taskid;

        return $this->getRequest('task.item.defer', $parameters);
    }

    /**
     * Delegates task to a user.
     *
     * @return boolean
     */
    public function item_delegate(int $taskid, int $userid)
    {
        $parameters['taskid'] = $taskid;
        $parameters['userid'] = $userid;

        return $this->getRequest('task.item.delegate', $parameters);
    }

    /**
     * Deletes a task.
     *
     * @return boolean
     */
    public function item_delete(int $taskid)
    {
        $parameters['taskid'] = $taskid;

        return $this->getRequest('task.item.delete', $parameters);
    }

    /**
     * This method deletes attachment between file and task.
     *
     * @return array
     */
    public function item_deletefile(int $taskid, int $attachment_id)
    {
        $parameters['TASK_ID'] = $taskid;
        $parameters['ATTACHMENT_ID'] = $attachment_id;

        return $this->getRequest('task.item.deletefile', $parameters);
    }

    /**
     * This method deletes task from Favorites.
     *
     * @return array
     */
    public function item_deletefromfavorite(int $taskid, array $params)
    {
        $parameters['TASK_ID'] = $taskid;
        $parameters['PARAMS'] = $params;

        return $this->getRequest('task.item.deletefromfavorite', $parameters);
    }

    /**
     * Changes status of task waiting for confirmation to Pending.
     *
     * @return array
     */
    public function item_disapprove(int $taskid)
    {
        $parameters['taskid'] = $taskid;

        return $this->getRequest('task.item.disapprove', $parameters);
    }

    /**
     * Returns an array of allowed task actions IDs (see PHP class constants CTaskItem).
     *
     * @return array
     */
    public function item_getallowedactions(int $id)
    {
        $parameters['id'] = $id;

        return $this->getRequest('task.item.getallowedactions', $parameters);
    }

    /**
     * Returns an array whose keys are acton names (the names correspond to PHP class constants CTaskItem)
     * and values show whether the action is allowed (true) or not allowed (false).
     *
     * @return array
     */
    public function item_getallowedtaskactionsasstrings(int $id)
    {
        $parameters['id'] = $id;

        return $this->getRequest('task.item.getallowedtaskactionsasstrings', $parameters);
    }

    /**
     * Returns an array of task data fields (TITLE, DESCRIPTION, etc.). The following fields are available.
     *
     * @return array
     */
    public function item_getdata(int $taskid)
    {
        $parameters['taskid'] = $taskid;

        return $this->getRequest('task.item.getdata', $parameters);
    }

    /**
     * Returns an array with task IDs, on which the task depends (Previous tasks option in task creation form).
     *
     * @return array
     */
    public function item_getdependson(int $id)
    {
        $parameters['id'] = $id;

        return $this->getRequest('task.item.getdependson', $parameters);
    }

    /**
     * Returns task description.
     *
     * @return array
     */
    public function item_getdescription(int $id, int $format = 1)
    {
        $parameters['id'] = $id;
        $parameters['format'] = $format;

        return $this->getRequest('task.item.getdescription', $parameters);
    }

    /**
     * Returns an array of links to files attached to the task.
     *
     * @return array
     */
    public function item_getfiles(int $taskid)
    {
        $parameters['taskid'] = $taskid;

        return $this->getRequest('task.item.getfiles', $parameters);
    }

    /**
     * Returns list of methods of the task.item.* type and their description.
     * The returned value of this method cannot be processed automatically because its format may be changed without notice.
     * This method may be useful as reference because it always contains updated information.
     *
     * @return array
     */
    public function item_getmanifest()
    {
        return $this->getRequest('task.item.getmanifest');
    }

    /**
     * Returns true if action is allowed, otherwise returns false.
     *
     * @return boolean
     */
    public function item_isactionallowed(int $taskid, int $actionid)
    {
        $parameters['taskid'] = $taskid;
        $parameters['actionid'] = $actionid;

        return $this->getRequest('task.item.isactionallowed', $parameters);
    }

    /**
     * Returns an array of tasks, each of which contains a fields array (similar to the array, returned by task.item.getdata).
     *
     * @return array
     */
    public function item_list(array $order = [], array $filter = [], array $params = [], array $select = [])
    {
        $parameters['order'] = $order;
        $parameters['filter'] = $filter;
        $parameters['params'] = $params;
        $parameters['select'] = $select;

        return $this->getRequest('task.item.list', $parameters);
    }

    /**
     * Changes status to Pending.
     *
     * @return array
     */
    public function item_renew(int $taskid)
    {
        $parameters['taskid'] = $taskid;

        return $this->getRequest('task.item.renew', $parameters);
    }

    /**
     * Changes task status to In Progress.
     *
     * @return array
     */
    public function item_startexecution(int $taskid)
    {
        $parameters['taskid'] = $taskid;

        return $this->getRequest('task.item.startexecution', $parameters);
    }

    /**
     * Updates task data. The following fields may be updated.
     * Business logic and permission rights are taken into account when updating task data.
     * For example: The person responsible cannot rename a task. An error will be generated in this case.
     * Prior to data update, it is recommended to check whether this action is allowed (task.item.isactionallowed).
     *
     * @return array
     */
    public function item_update(int $id, array $fields)
    {
        $parameters['id'] = $id;
        $parameters['fields'] = $fields;

        return $this->getRequest('task.item.update', $parameters);
    }


}
<?php

namespace App\Repositories\Bitrix24;

use App\Traits\GuzzleHttpRequest;

class Telephony
{
    use GuzzleHttpRequest;

    /**
     * Method registers a call in Bitrix24. For this purpose, it searches an object that corresponds to the number in CRM.
     * If the method finds it, it adds the call, connected to the object found.
     * If the method didn't find it, it can create a lead automatically.
     * Simultaneously with the registration of a call, method optionally can show the user the call ID screen.
     * The user, to who the card is shown, is identified either by USER_ID, or by USER_PHONE_INNER.
     * (That is, fields are marked as required, but actually, only one of two is required.)
     * Method is accessible to the owner of the access right for Call statistics - modification, if granted.
     * To create a call "activity", it is also necessary to call the telephony.externalcall.finish method
     *
     * @return array
     */
    public function externalcall_register(int $user_id, string $phone_number, string $user_phone_inner = '', int $type = 3)
    {
        $parameters['USER_ID'] = $user_id;
        $parameters['PHONE_NUMBER'] = $phone_number;
        $parameters['USER_PHONE_INNER'] = $user_phone_inner;
        $parameters['TYPE'] = $type;

        return $this->getRequest('telephony.externalcall.register', $parameters);
    }

    /**
     * This method displays a call ID screen to the user.
     *
     * @return array
     */
    public function externalcall_show(string $call_id, $user_id)
    {
        $parameters['CALL_ID'] = $call_id;
        $parameters['USER_ID'] = $user_id;

        return $this->getRequest('telephony.externalcall.show', $parameters);
    }

    /**
     * Method allows to retrieve the list of external lines of an application. 
     *
     * @return array
     */
    public function externalline_get()
    {
        return $this->getRequest('telephony.externalLine.get');
    }


}
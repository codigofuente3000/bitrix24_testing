<?php

namespace App\Console\Commands;

use App\Models\Setting;
use GuzzleHttp\Client;
use Illuminate\Console\Command;

class RefreshToken extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'b24:refresh';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Refresh Bitrix24 Access Token';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->info("   Refreshing Access Token...");

        $time_start = microtime(true); 

        $client_id = config('bitrix24.client_id');
        $client_secret = config('bitrix24.client_secret');
        $refresh_token = Setting::get('bitrix24.refresh_token');

        $this->client = new Client([
            'base_uri'  => 'https://tecnosyste.bitrix24.es/oauth/token/',
                'query'     => [
                    'grant_type'    => 'refresh_token',
                    'client_id'     => $client_id,
                    'client_secret' => $client_secret,
                    'refresh_token' => $refresh_token,
                    'scope'         => 'granted_permission',
                    'redirect_uri'  => 'app_URL',
                ],
        ]);

        $response = $this->client->request('GET');

        $data = json_decode($response->getBody()->getContents());

        Setting::set('bitrix24.access_token', $data->access_token);
        Setting::set('bitrix24.expires', $data->expires);
        Setting::set('bitrix24.expires_in', $data->expires_in);
        Setting::set('bitrix24.scope', $data->scope);
        Setting::set('bitrix24.domain', $data->domain);
        Setting::set('bitrix24.server_endpoint', $data->server_endpoint);
        Setting::set('bitrix24.status', $data->status);
        Setting::set('bitrix24.client_endpoint', $data->client_endpoint);
        Setting::set('bitrix24.user_id', $data->user_id);
        Setting::set('bitrix24.refresh_token', $data->refresh_token);

        $execution_time = round(microtime(true) - $time_start, 2);

        $this->info("   Access Token Refreshed in {$execution_time} seconds.");

    }

}

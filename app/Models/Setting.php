<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Setting extends Model
{
    protected $table = 'settings';

    protected $guarded = [];

    public $timestamps = false;

    public static function get($key, $default = null)
    {
    	$setting = Self::where('key', $key)->first();
    	return isset($setting) ? $setting->getAttributeValue('value') : $default;
    }

    public static function set($key, $value)
    {
    	$setting = Self::where('key', $key)->firstOrFail();
    	$setting->value = $value;
		$setting->save();
    }
}

<?php

namespace App\Traits;

use App\Models\Setting;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;

trait GuzzleHttpRequest
{
    protected $client;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Client $client)
    {
        $this->client = $client;
    }

    protected function getRequest($uri, $parameters = [])
    {
        $parameters['auth'] = Setting::get('bitrix24.access_token');

        $response = $this->client->request('GET', $uri, ['query' => $parameters]);

        return json_decode($response->getBody()->getContents(), true);
    }

}
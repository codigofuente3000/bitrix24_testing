<?php

use App\Models\Setting;
use Illuminate\Database\Seeder;

class Bitrix24SettingsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $credentials = config('bitrix24.credentials');

        $setting = Setting::firstOrNew(['key' => 'bitrix24.access_token']);
        if (!$setting->exists) {
            $setting->fill([
                'group'        => 'Bitrix24',
                'order'        => 1,
                'display_name' => 'Bitrix24 Access Token',
                'value'        => $credentials['access_token'],
                'type'         => 'text',
            ])->save();
        }

        $setting = Setting::firstOrNew(['key' => 'bitrix24.expires']);
        if (!$setting->exists) {
            $setting->fill([
                'group'        => 'Bitrix24',
                'order'        => 2,
                'display_name' => 'Bitrix24 Expires',
                'value'        => $credentials['expires'],
                'type'         => 'text',
            ])->save();
        }

        $setting = Setting::firstOrNew(['key' => 'bitrix24.expires_in']);
        if (!$setting->exists) {
            $setting->fill([
                'group'        => 'Bitrix24',
                'order'        => 3,
                'display_name' => 'Bitrix24 Expires In',
                'value'        => $credentials['expires_in'],
                'type'         => 'text',
            ])->save();
        }

        $setting = Setting::firstOrNew(['key' => 'bitrix24.scope']);
        if (!$setting->exists) {
            $setting->fill([
                'group'        => 'Bitrix24',
                'order'        => 4,
                'display_name' => 'Bitrix24 Scope',
                'value'        => $credentials['scope'],
                'type'         => 'text',
            ])->save();
        }

        $setting = Setting::firstOrNew(['key' => 'bitrix24.domain']);
        if (!$setting->exists) {
            $setting->fill([
                'group'        => 'Bitrix24',
                'order'        => 5,
                'display_name' => 'Bitrix24 Domain',
                'value'        => $credentials['domain'],
                'type'         => 'text',
            ])->save();
        }

        $setting = Setting::firstOrNew(['key' => 'bitrix24.server_endpoint']);
        if (!$setting->exists) {
            $setting->fill([
                'group'        => 'Bitrix24',
                'order'        => 6,
                'display_name' => 'Bitrix24 Server End',
                'value'        => $credentials['server_endpoint'],
                'type'         => 'text',
            ])->save();
        }

        $setting = Setting::firstOrNew(['key' => 'bitrix24.status']);
        if (!$setting->exists) {
            $setting->fill([
                'group'        => 'Bitrix24',
                'order'        => 7,
                'display_name' => 'Bitrix24 Status',
                'value'        => $credentials['status'],
                'type'         => 'text',
            ])->save();
        }

        $setting = Setting::firstOrNew(['key' => 'bitrix24.client_endpoint']);
        if (!$setting->exists) {
            $setting->fill([
                'group'        => 'Bitrix24',
                'order'        => 8,
                'display_name' => 'Bitrix24 Client Endpoint',
                'value'        => $credentials['client_endpoint'],
                'type'         => 'text',
            ])->save();
        }

        $setting = Setting::firstOrNew(['key' => 'bitrix24.member_id']);
        if (!$setting->exists) {
            $setting->fill([
                'group'        => 'Bitrix24',
                'order'        => 9,
                'display_name' => 'Bitrix24 Member ID',
                'value'        => $credentials['member_id'],
                'type'         => 'text',
            ])->save();
        }

        $setting = Setting::firstOrNew(['key' => 'bitrix24.user_id']);
        if (!$setting->exists) {
            $setting->fill([
                'group'        => 'Bitrix24',
                'order'        => 10,
                'display_name' => 'Bitrix24 User ID',
                'value'        => $credentials['user_id'],
                'type'         => 'text',
            ])->save();
        }

        $setting = Setting::firstOrNew(['key' => 'bitrix24.refresh_token']);
        if (!$setting->exists) {
            $setting->fill([
                'group'        => 'Bitrix24',
                'order'        => 11,
                'display_name' => 'Bitrix24 Refresh Token',
                'value'        => $credentials['refresh_token'],
                'type'         => 'text',
            ])->save();
        }

        
    }
}
